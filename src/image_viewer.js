//this module does not export any code
import small from '../assets/small_600_600.jpg';
import '../styles/image_viewer.css';

export default () => {
    const imageSmall = document.createElement('img');
    imageSmall.src = small;
    document.body.appendChild(imageSmall);
};
